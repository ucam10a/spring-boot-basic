package com.yung.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.yung.StartWebApplication;
import com.yung.tool.FileUtil;
import com.yung.tool.TraceTool;
import com.yung.web.test.App;
import com.yung.web.test.RPACommandClient;

@RestController
@RequestMapping(path = "/rest")
public class RestFulController 
{
 
    private static final Logger logger = LoggerFactory.getLogger(RestFulController.class);
    private TraceTool traceTool = TraceTool.getTraceTool(logger, false);
    
    @PostMapping(path="/viewRPALog", produces="application/json")
    public Map<String, Object> viewRPALog(@RequestParam(name = "jarName") String jarName)
    {
        Map<String, Object> ret = new HashMap<String, Object>();
        try {
            
            String logMessage = "";
            String baseDir = StartWebApplication.getRPAJarPath();
            traceTool.reflectTrace("viewRPALog", "baseDir", baseDir);
            String driverLogs = baseDir + "/";
            traceTool.reflectTrace("viewRPALog", "driverLogs", driverLogs);
            String rpaName = RPACommandClient.getRPAName(jarName);
            traceTool.reflectTrace("viewRPALog", "rpaName", rpaName);
            String rpaDirPath = App.getRPADir(driverLogs, rpaName);
            traceTool.reflectTrace("viewRPALog", "rpaDirPath", rpaDirPath);
            File tempLog = findtempLog(rpaDirPath);
            File logFile = null;
            if (tempLog == null) {
                // all finished then get latest one
                logFile = findLatestLog(rpaDirPath);
            }
            if (tempLog == null || !tempLog.exists()) {
                if (logFile == null || !logFile.exists()) {
                    logMessage = "RPA Process is running!" + RPACommandClient.LINE_SEPARATOR;
                    ret.put("errormsg", "");
                    ret.put("logMessage", logMessage);
                    return ret;
                }
            }
            if (tempLog != null && tempLog.exists()) {
                logMessage = readLog(tempLog);
            } else {
                logMessage = readLog(logFile);
            }
            if (logFile != null && logFile.exists()) {
                logMessage = logMessage + "RPA Process finished!" + RPACommandClient.LINE_SEPARATOR;
            } else {
                logMessage = logMessage + "RPA Process is running!" + RPACommandClient.LINE_SEPARATOR;
            }
            
            ret.put("errormsg", "");
            ret.put("logMessage", logMessage);
            return ret;
        } catch (Exception e) {
            logger.error(e.toString(), e);
            ret.put("errormsg", e.toString());
        }
        return ret;
        
    }
    
    private File findLatestLog(String rpaDirPath) {
        TreeMap<String, File> treeMap = new TreeMap<String, File>();
        File rpaDir = FileUtil.getFile(rpaDirPath);
        if (rpaDir.exists()) {
            File[] logDirs = rpaDir.listFiles();
            if (logDirs != null && logDirs.length > 0) {
                for (File logDir : logDirs) {
                    File logFile = RPACommandClient.getFileWithPrefix(logDir, App.SERVER_APP_NAME);
                    if (logFile != null && logFile.exists()) {
                        treeMap.put(logFile.getName(), logFile);
                    }
                }
            }
        }
        return treeMap.lastEntry().getValue();
    }

    private File findtempLog(String rpaDirPath) {
        File ret = null;
        File rpaDir = FileUtil.getFile(rpaDirPath);
        if (rpaDir.exists()) {
            File[] logDirs = rpaDir.listFiles();
            if (logDirs != null && logDirs.length > 0) {
                for (File logDir : logDirs) {
                    File tempLog = FileUtil.getFile(logDir.getAbsolutePath() + "/" + App.TEMP_LOG_NAME);
                    if (tempLog.exists()) {
                        ret = tempLog;
                    }
                }
            }
        }
        return ret;
    }

    private String readLog(File tempLog) throws IOException {
        StringBuilder bud = new StringBuilder();
        BufferedReader br = new BufferedReader(new FileReader(tempLog));
        try {
            String line = br.readLine();
            while (line != null) {
                if (line != null && !line.equals("")) bud.append(line + RPACommandClient.LINE_SEPARATOR);
                line = br.readLine();
            }
        } finally {
            br.close();
        }
        return bud.toString();
    }
    
    @PostMapping(path="/cancel", produces="application/json")
    public Map<String, Object> cancel(@RequestParam(name = "jarName") String jarName, @RequestParam(name = "pid") String pid, @RequestParam(name = "logNo") String logNo)
    {
        Map<String, Object> ret = new HashMap<String, Object>();
        try {
            if (pid == null || "".equals(pid)) {
                throw new RuntimeException("pid is empty!");
            }
            if (logNo == null || "".equals(logNo)) {
                throw new RuntimeException("logNo is empty!");
            }
            File jarFile = FileUtil.getFile(StartWebApplication.getRPAJarPath() + "/" + jarName);
            RPACommandClient.cancelProcess(Integer.valueOf(pid), logNo, jarFile);
            ret.put("errormsg", "");
            return ret;
        } catch (Exception e) {
            logger.error(e.toString(), e);
            ret.put("errormsg", e.toString());
        }
        return ret;
        
    }
    
}
