package com.yung.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.yung.ApplicationContextManager;
import com.yung.StartWebApplication;
import com.yung.tool.AbstractDebugPrinter;
import com.yung.tool.FileUtil;
import com.yung.tool.IpAddressUtils;
import com.yung.tool.TraceTool;
import com.yung.web.test.App;
import com.yung.web.test.AppConfig;
import com.yung.web.test.RPACommandClient;
import com.yung.web.test.RPAParameter;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.http.HttpServletRequest;

/**
 * Use Tomcat 8 above
 * 
 * 
 * @author Yung-Long Li
 *
 */
@Controller
public class WelcomeController {

    private static final Logger log = LoggerFactory.getLogger(WelcomeController.class);
    private static final TraceTool traceTool = TraceTool.getTraceTool(log, true);
    
    @Autowired
    private HttpServletRequest request;
    
    private String getRequestIpAddress() {
        String ipAddress = request.getRemoteAddr();
        if (IpAddressUtils.isPrivateIP(ipAddress)) {
            String remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (remoteAddr != null || !"".equals(remoteAddr)) {
                ipAddress = request.getRemoteAddr();
            }
        }
        return ipAddress;
    }
    
    @GetMapping("/")
    public String main(Model model) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        
        String color = ApplicationContextManager.getServletContext().getInitParameter("backgroundColor");
        log.info("color: " + color);
        
        String ipAddress = getRequestIpAddress();
        traceTool.reflectTrace("main", "ipAddress", ipAddress);
        model.addAttribute("ipAddress", ipAddress);
        //view
        return "welcome";
    }

    @GetMapping("/welcome")
    public String mainWithParam(@RequestParam(name = "name", required = false, defaultValue = "") String name, Model model) {

        String ipAddress = getRequestIpAddress();
        log.info("mainWithParam start ...");
        model.addAttribute("ipAddress", ipAddress);
        log.info("mainWithParam end");
        //view
        return "welcome";
        
    }
    
    @GetMapping("/runTest")
    public String runTest(@RequestParam(name = "jarName") String jarName, @RequestParam(name = "lang", required = false) String lang, @RequestParam(name = "logNo", required = false) String logNo, @RequestParam(name = "maxWaitSec", required = false) String maxWaitSec, @RequestParam(name = "headless", required = false) String headless, @RequestParam(name = "remoteURL", required = false) String remoteURL, Model model) throws IOException, URISyntaxException, InterruptedException {

        model.addAttribute("jarName", jarName);
        String htmlMsg = "";
        if (lang == null || "".equals(lang)) {
            lang = "en";
        }
        log.info("lang: " + lang);
        
        if (logNo == null) {
            logNo = System.currentTimeMillis() + "";
        }
        log.info("logNo: " + logNo);
        model.addAttribute("logNo", logNo);
        
        log.info("maxWaitSec: " + maxWaitSec);
        int waitSec = 1800;
        if (maxWaitSec != null) {
            try {
                waitSec = Integer.valueOf(maxWaitSec);
                if (waitSec < 20) {
                    log.info("waitSec minimum is 20 seconds!");
                    waitSec = 20;
                }
            } catch (Exception e) {
                log.warn(e.toString(), e);
            }
        }
        log.info("waitSec: " + waitSec);
        if (headless == null) {
            headless = "true";
        }
        log.info("headless: " + headless);
        
        String rpaJarPath = StartWebApplication.getRPAJarPath();
        traceTool.reflectTrace("runTest", "rpaJarPath", rpaJarPath);
        
        final File jarFile = FileUtil.getFile(rpaJarPath + "/" + jarName);
        if (!jarFile.exists()) {
            model.addAttribute("failMsg", "jarFile:" + jarFile.getAbsolutePath() + " not found!");
            return "fail";
        }
        
        String rpaName = RPACommandClient.getRPAName(jarName);
        traceTool.reflectTrace("runTest", "rpaName", rpaName);
        
        final AppConfig config = new AppConfig();
        config.setMaxWaitSec(waitSec);
        config.setHeadless(Boolean.valueOf(headless));
        config.setRpaName(rpaName);
        config.setRemoteUrl(remoteURL);
        traceTool.reflectTrace("runTest", "config", config);
        
        final RPAParameter parameter = new RPAParameter();
        synchronized(RPACommandClient.getPidMap()) {
        	
        	traceTool.reflectTrace("runTest", "pidMap", RPACommandClient.getPidMap());
        	
        	//check if temp-log.txt exist and ServerTestApp log not exist
            String driverLogs = rpaJarPath + "/";
            traceTool.reflectTrace("runTest", "driverLogs", driverLogs);
            String rpaDirPath = App.getRPADir(driverLogs, rpaName);
            traceTool.reflectTrace("runTest", "rpaDirPath", rpaDirPath);
            File rpaDir = FileUtil.getFile(rpaDirPath);
            int pid = findPid(RPACommandClient.getPidMap(), jarFile.getAbsolutePath());
            log.info("pid: " + pid + ", for " + jarFile.getAbsolutePath());
            if (pid > 0) {
            	RPAParameter rpaParameter = RPACommandClient.getPidMap().get(pid);
                model.addAttribute("jarName", jarFile.getName());
                model.addAttribute("logNo", rpaParameter.getRunId());
                model.addAttribute("pid", rpaParameter.getProcessId());
                StringBuilder sb = new StringBuilder();
                sb.append("RPA jar:" + jarFile.getAbsolutePath() + " is running!" + RPACommandClient.LINE_SEPARATOR);
                sb.append("Please wait for notification when RPA process finished!" + RPACommandClient.LINE_SEPARATOR);
                sb.append("or click 'View Log' to view log." + RPACommandClient.LINE_SEPARATOR);
                sb.append("<a href='javascript:void(null);' onclick='viewLog();' >View log</a> ");
                sb.append("<a href='javascript:void(null);' onclick='cancel(" + pid + ");' >Cancel</a>" + RPACommandClient.LINE_SEPARATOR);
                htmlMsg = AbstractDebugPrinter.changeToHtml(sb.toString());
                model.addAttribute("htmlMsg", htmlMsg);
                return "rpa";
            } else {
            	// health check
            	if (rpaDir.exists()) {
                    File[] logDirs = rpaDir.listFiles();
                    if (logDirs != null && logDirs.length > 0) {
                        for (File logDir : logDirs) {
                            File tempLog = FileUtil.getFile(logDir.getAbsolutePath() + "/" + App.TEMP_LOG_NAME);
                            File logFile = RPACommandClient.getFileWithPrefix(logDir, App.SERVER_APP_NAME);
                            if (tempLog.exists()) {
                            	// pid not exist, then delete temp log folder
                                if (logFile != null && logFile.exists()) {
                                	// delete tempLog
                                	FileUtil.delete(tempLog.getAbsolutePath());
                                } else {
                                	// delete folder
                                	FileUtil.delete(tempLog.getParent());
                                }
                            }
                        }
                    }
                }
            }
            
            final String runId = logNo;
            final String i18n = lang;
            final Object controller = this;
            ExecutorService executorService = Executors.newScheduledThreadPool(1);
            executorService.execute(new Runnable() {
                public void run() {
                    try {
                        traceTool.trace("start to RPACommandClient.exeRPACommand ...");
                        String msg = RPACommandClient.exeRPACommand(controller, parameter, jarFile, runId, config, i18n);
                        // TODO send mail
                        log.info(msg);
                    } catch (Exception e) {
                        log.error(e.toString(), e);
                        // TODO send mail
                    }
                }
            });
            executorService.shutdown();
            // wait at most 20 seconds for pid
            for (int i = 0; i < 20; i++) {
                Thread.sleep(1000);
                int processId = parameter.getProcessId();
                if (processId != 0 && RPACommandClient.getPidMap().containsKey(processId)) {
                    break;
                }
            }
        	
        }
        
        StringBuilder sb = new StringBuilder();
        sb.append("RPA jar:" + jarFile.getAbsolutePath() + " executed successfaully!" + RPACommandClient.LINE_SEPARATOR);
        sb.append("Please wait for notification when RPA process finished!" + RPACommandClient.LINE_SEPARATOR);
        sb.append("or click 'View Log' to view log." + RPACommandClient.LINE_SEPARATOR);
        sb.append("<a href='javascript:void(null);' onclick='viewLog();' >View log</a> ");
        sb.append("<a href='javascript:void(null);' onclick='cancel(" + parameter.getProcessId() + ");' >Cancel</a>" + RPACommandClient.LINE_SEPARATOR);
        htmlMsg = AbstractDebugPrinter.changeToHtml(sb.toString());
        
        model.addAttribute("jarName", jarFile.getName());
        model.addAttribute("logNo", parameter.getRunId());
        model.addAttribute("pid", parameter.getProcessId());
        model.addAttribute("htmlMsg", htmlMsg);
        return "rpa";
        
    }

    private int findPid(ConcurrentHashMap<Integer, RPAParameter> pidMap, String jarFilePath) {
        for (Entry<Integer, RPAParameter> entry : pidMap.entrySet()) {
            if (entry.getValue().getJarFilePath().equals(jarFilePath)) {
                return entry.getKey();
            }
        }
        return -1;
    }
    
    @GetMapping("/delTest")
    public String delTest(@RequestParam(name = "jarName") String jarName, Model model) throws URISyntaxException, IOException {
        
        String rpaName = RPACommandClient.getRPAName(jarName);
        for (Entry<Integer, RPAParameter> entry : RPACommandClient.getPidMap().entrySet()) {
            String jarFilePath = entry.getValue().getJarFilePath();
            File jarFile = FileUtil.getFile(jarFilePath);
            if (jarFile.getName().equals(jarName)) {
                model.addAttribute("failMsg", "jarName: " + jarName + " is runnung, please wait!");
                return "fail";
            }
        }
        String rpaJarPath = StartWebApplication.getRPAJarPath();
        String driverLogs = rpaJarPath + "/";
        traceTool.reflectTrace("runTest", "driverLogs", driverLogs);
        String rpaDirPath = App.getRPADir(driverLogs, rpaName);
        FileUtil.delete(rpaDirPath);
        FileUtil.generateDir(rpaDirPath);
        model.addAttribute("msg", "delete test: " + jarName + " successfully!");
        return "msg";
        
    }
    
}