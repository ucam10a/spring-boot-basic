package com.yung.entity;

import java.math.BigDecimal;

public class Schema {

	private boolean key;
	
	private String embedIdField;
	
	private String name;
	
	private String type;
	
	private int length;
	
	private String defaultString;
	
	private BigDecimal defaultNumber;
	
	private boolean allowNull = false;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getDefaultString() {
		return defaultString;
	}

	public void setDefaultString(String defaultString) {
		this.defaultString = defaultString;
	}

	public boolean isAllowNull() {
		return allowNull;
	}

	public void setAllowNull(boolean allowNull) {
		this.allowNull = allowNull;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getDefaultNumber() {
		return defaultNumber;
	}

	public void setDefaultNumber(BigDecimal defaultNumber) {
		this.defaultNumber = defaultNumber;
	}

	public boolean isKey() {
		return key;
	}

	public void setKey(boolean key) {
		this.key = key;
	}

	public String getEmbedIdField() {
		return embedIdField;
	}

	public void setEmbedIdField(String embedIdField) {
		this.embedIdField = embedIdField;
	}
	
}