package com.yung.entity.ann;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The custom annotation for POJOReprotBean field
 * 
 * @author Yung Long Li
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
// on field level
public @interface SchemaInfo {

    /** type name, ex. string, number, date, clob */
    String type() default "string";

    /** varchar, ex. -1 mean no limit */
    int length() default -1;
    
    /** allow null */
    boolean allowNull() default false;
    
    /** default string */
    String defaultString() default " ";
    
    /** default string */
    double defaultNumber() default 0;
    
    /** default UTC time stamp */
    String defaultTime() default "1900-01-01T00:00:00.000Z";
    
}