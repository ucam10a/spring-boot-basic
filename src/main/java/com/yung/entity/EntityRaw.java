package com.yung.entity;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Id;
import javax.persistence.Table;

import com.yung.entity.ann.SchemaInfo;
import com.yung.tool.PlainObjectOperator;

public interface EntityRaw {

	public String getEntityInfo();
	
	public static String getEntityTableName(Class<?> cls) {
		Table tbl = cls.getAnnotation(Table.class);
		if (tbl != null) {
			return tbl.name();
		}
		return cls.getSimpleName();
	}
	
	public static String getEntityKeyValue(Class<?> cls, Object entity) {
		try {
			StringBuilder sb = new StringBuilder();
			Field[] fields = cls.getDeclaredFields();
			for (Field f : fields) {
				Id id = f.getAnnotation(Id.class);
				if (id != null) {
					Object val = PlainObjectOperator.runGetter(f, entity);
					if (val == null) {
						throw new Exception("key field: " + f.getName() + " value is null");
					}
					sb.append(f.getName() + ":'" + val + "',");
				}
			}
			for (Field f : fields) {
	            EmbeddedId e = f.getAnnotation(EmbeddedId.class);
	            if (e != null) {
	            	Object id = PlainObjectOperator.runGetter(f, entity);
	                Class<?> idCls = f.getType();
	                Field[] keyFields = idCls.getDeclaredFields();
	                for (Field kf : keyFields) {
	                	Column c = kf.getAnnotation(Column.class);
	                	if (c != null) {
	                		Object val = PlainObjectOperator.runGetter(kf, id);
		                    if (val == null) {
		                        throw new Exception("key field: " + kf.getName() + " value is null");
		                    }
		                    sb.append(kf.getName() + ":'" + val + "',");
	                	}
	                }
	                break;
	            }
			}
			String ret = sb.toString();
			if (ret.length() > 0) {
				int length = ret.length();
				return "<" + ret.substring(0, length - 1) + ">";
			}
			return null;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static Map<String, Schema> getSchemaMap(Class<?> cls) {
		Map<String, Schema> ret = new HashMap<String, Schema>();
		Field[] fields = cls.getDeclaredFields();
		for (Field f : fields) {
			SchemaInfo s = f.getAnnotation(SchemaInfo.class);
			Id isKey = f.getAnnotation(Id.class);
			if (s != null) {
				Schema sch = new Schema();
				sch.setName(f.getName());
				sch.setType(s.type());
				sch.setLength(s.length());
				sch.setAllowNull(s.allowNull());
				sch.setDefaultString(s.defaultString());
				sch.setDefaultNumber(new BigDecimal(s.defaultNumber()));
				if (isKey != null) {
					sch.setKey(true);
				}
				ret.put(f.getName(), sch);
			}
		}
		for (Field f : fields) {
		    EmbeddedId e = f.getAnnotation(EmbeddedId.class);
            if (e != null) {
                Class<?> idCls = f.getType();
                Field[] keyFields = idCls.getDeclaredFields();
                for (Field kf : keyFields) {
                    SchemaInfo s = kf.getAnnotation(SchemaInfo.class);
                    if (s != null) {
                        Schema sch = new Schema();
                        sch.setName(kf.getName());
                        sch.setType(s.type());
                        sch.setLength(s.length());
                        sch.setAllowNull(s.allowNull());
                        sch.setDefaultString(s.defaultString());
                        sch.setDefaultNumber(new BigDecimal(s.defaultNumber()));
                        sch.setKey(true);
                        sch.setEmbedIdField(f.getName());
                        ret.put(kf.getName(), sch);
                    }
                }
                break;
            }
		}
		return ret;
	}
}