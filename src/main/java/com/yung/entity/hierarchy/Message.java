package com.yung.entity.hierarchy;

public class Message {

    private boolean allowFire = true;
    
    private String errormsg = "";

    public boolean isAllowFire() {
        return allowFire;
    }

    public void setAllowFire(boolean allowFire) {
        this.allowFire = allowFire;
    }

    public String getErrormsg() {
        return errormsg;
    }

    public void setErrormsg(String errormsg) {
        this.errormsg = errormsg;
    }
    
}
