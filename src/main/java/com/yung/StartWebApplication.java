package com.yung;

import java.io.File;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.context.ServletContextAware;

import com.yung.web.test.FileUtil;

@SpringBootApplication
@ServletComponentScan
public class StartWebApplication extends SpringBootServletInitializer implements ServletContextAware {

    private static final Logger logger = LoggerFactory.getLogger(StartWebApplication.class);
    
    @Autowired
    private ApplicationContext context;
    
    @PostConstruct
    public void init() {
        ApplicationContextManager.setContext(context);
    }
    
    public static void main(String[] args) {
        String currentBase = new File("").getAbsolutePath();
        System.setProperty("current.base", currentBase);
        logger.info("current.base: " + currentBase);
        SpringApplication.run(StartWebApplication.class, args);
    }

    public static String getRPAJarPath() {
        String currentBase = System.getProperty("current.base");
        String parent = FileUtil.getFile(currentBase).getParent();
        return parent + "/jars";
    }
    
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(StartWebApplication.class);
    }
    
    @Bean
    public ServletContextInitializer initializer() {
        return new ServletContextInitializer() {
            @Override
            public void onStartup(ServletContext servletContext) throws ServletException {
                servletContext.setInitParameter("backgroundColor", "red");
            }
        };
    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        ApplicationContextManager.setServletContext(servletContext);
    }
    
}