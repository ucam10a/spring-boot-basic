package com.yung.tool;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class SecretTool {
	
	private static final AppLogger logger = AppLogger.getLogger(SecretTool.class);

	private static final int TSP_LENGTH = 13;
	private static final int ENCODE_TSP_LENGTH = 20;
	
	private static final int DEFAULT_ALLOW_SECONDS = 15 * 60;
	
    private static String getPostfix(String tsp, String key) {
        return getSHA256(tsp + key);
    }
    
    public static String getSHA256(String input) {
        MessageDigest messageDigest;
        String encodeStr = "";
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(input.getBytes("UTF-8"));
            encodeStr = byte2Hex(messageDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return encodeStr;
    }
    
    private static String byte2Hex(byte[] bytes) {
        StringBuffer stringBuffer = new StringBuffer();
        String temp = null;
        for (int i = 0; i < bytes.length; i++) {
            temp = Integer.toHexString(bytes[i] & 0xFF);
            if (temp.length() == 1) {

                stringBuffer.append("0");
            }
            stringBuffer.append(temp);
        }
        return stringBuffer.toString();
    }
    
    private static String reverseString(String input) {
        StringBuilder sb = new StringBuilder();
        if (input == null) return null; 
        for (int i = input.length() - 1; i >= 0; i--) {
            sb.append(input.charAt(i));
        }
        return sb.toString();
    }
    
    private static String mixWithTimestamp(String tsp, String input) {
        String encodeTsp = createEncodeTsp(tsp);
        String reverseTsp = reverseString(encodeTsp);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);
            String tspCh = "";
            if (i < reverseTsp.length()) {
                tspCh = reverseTsp.charAt(i) + "";
            }
            sb.append(ch).append(tspCh);
        }
        return sb.toString();
    }
    
    private static String createEncodeTsp(String tsp) {
        int len = ENCODE_TSP_LENGTH - TSP_LENGTH;
    	String prefix = tsp.substring(0, len);
    	int prefixInt = Integer.valueOf(prefix);
    	int max = getMax(len);
    	int range = max - prefixInt;
    	Random random = new Random();
    	long seed = random.nextInt(range);
    	String tmp = String.format("%02d", len);
    	String prefixSeed = String.format("%" + tmp + "d", seed);
    	long t1 = Long.valueOf(tsp);
    	int base = getBase(TSP_LENGTH - len);
    	long t2 = seed * base;
    	long encodeTsp = t1 + t2;
    	return prefixSeed + encodeTsp;
	}
    
    private static int getBase(int scale) {
        StringBuilder sb = new StringBuilder();
        sb.append("1");
        for (int i = 0; i < scale; i++) {
            sb.append("0");
        }
        return Integer.valueOf(sb.toString());
    }

    private static int getMax(int len) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < len; i++) {
            sb.append("9");
        }
        return Integer.valueOf(sb.toString());
    }

    private static String decodeEncodeTsp(String encodeTsp) {
        int len = ENCODE_TSP_LENGTH - TSP_LENGTH;
		String prefix = encodeTsp.substring(0, len);
    	String addedTsp = encodeTsp.substring(len);
    	int base = getBase(TSP_LENGTH - len);
    	long source = Long.valueOf(addedTsp) - (Long.valueOf(prefix) * base);
    	return source + "";
	}

	private static long extractTsp(String input) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < ENCODE_TSP_LENGTH; i++) {
            int idx = i * 2 + 1;
            sb.append(input.charAt(idx));
        }
        String encodeTsp = reverseString(sb.toString());
        String tsp = decodeEncodeTsp(encodeTsp);
        long ret = Long.valueOf(tsp);
        return ret;
    }
	
	private static String extractSha256(String input) {
	    StringBuilder sb = new StringBuilder();
        for (int i = 0; i < ENCODE_TSP_LENGTH; i++) {
            int idx = i * 2;
            sb.append(input.charAt(idx));
        }
        String remain = input.substring(ENCODE_TSP_LENGTH * 2);
        sb.append(remain);
        return sb.toString();
    }
    
    private static long calculateLatency(long tsp) {
        return System.currentTimeMillis() - tsp;
    }
    
    private static boolean timeout(String mixTspInput, long allowLatency) {
        long sourceTsp = extractTsp(mixTspInput);
        long latency = calculateLatency(sourceTsp);
        if (latency <= allowLatency) {
            return false;
        } else {
            return true;
        }
    }
    
    private static boolean checkKey(String mixTspInput, String key) {
        long sourceTsp = extractTsp(mixTspInput);
        String sourceKey = extractSha256(mixTspInput);
        String checkKey = getSHA256(sourceTsp + key);
        if (sourceKey.equals(checkKey)) {
            return true;
        }
        return false;
    }
    
    public static String createSecret(String key) {
        try {
            Thread.sleep(5);
        } catch (InterruptedException e) {
            logger.debug(e.toString());
        }
        String tsp = System.currentTimeMillis() + "";
    	String source = getPostfix(tsp, key);
        String mix = mixWithTimestamp(tsp, source);
        return mix;
    }
    
    public static boolean validateSecret(String secret, String key, int allowSec) {
    	try {
    		boolean valid = !timeout(secret, allowSec * 1000);
    		if (valid) {
    		    if (checkKey(secret, key)) {
    	            return true;
    	        }
    	    }
    		return false;
    	} catch (Exception e) {
    		logger.debug(e.toString(), e);
    		return false;
    	}
    }
    
    public static boolean validateSecret(String secret, String key) {
    	return validateSecret(secret, key, DEFAULT_ALLOW_SECONDS);
    }
	
}