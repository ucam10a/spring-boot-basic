package com.yung.tool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCDataSource {

    public static Connection getConnection(String driver, String user, String password, String url) {
        Connection conn;
    	try {
            try {
                Class.forName(driver);
            } catch (Exception e) {
                System.out.println("ERROR: failed to load JDBC driver.");
                return null;
            }
            conn = DriverManager.getConnection(url, user, password);

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return conn;
    }

}