package com.yung.tool;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Odata key / value encoder
 * 
 * @author Yung-Long Li
 *
 */
public class OdataEncoder {

    private static final Map<String, String> codeMap = new HashMap<String, String>();
    
    static {
        codeMap.put(" ", "%20");
        codeMap.put("'", "%27");
        codeMap.put("<", "%3C");
        codeMap.put(">", "%3E");
    }
    
    private static boolean isASCII(String checkString) {
        if (checkString == null || "".equals(checkString.trim())) {
            return true;
        }
        CharsetEncoder asciiEncoder = Charset.forName("US-ASCII").newEncoder();
        return asciiEncoder.canEncode(checkString);
    }
    
    /**
     * odata input parameter 
     * 
     * @param input input string
     * @return encoded value
     * @throws UnsupportedEncodingException
     */
    public static String encode(String input) throws UnsupportedEncodingException {
        if (input == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            String ch = input.charAt(i) + "";
            if (isASCII(ch)) {
                if (codeMap.containsKey(ch)) {
                    sb.append(codeMap.get(ch));
                } else {
                    sb.append(ch);
                }
            } else {
                String ret = URLEncoder.encode(ch, "utf-8");
                sb.append(ret);
            }
        }
        return sb.toString();
    }
    
}
