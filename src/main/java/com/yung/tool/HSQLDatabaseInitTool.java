package com.yung.tool;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;

public class HSQLDatabaseInitTool {

	public void initiKOSDB(Connection conn) {

        try {

        	StringBuilder sb = new StringBuilder();
        	
            // connect to database and save data
            conn.setAutoCommit(false);

            Statement stmt = conn.createStatement();
            String sql = "";
            ArrayList<String> sqls = new ArrayList<String>();
            sqls.add("DROP TABLE IF EXISTS PS_COURSE_TBL; ");
            sqls.add("SET IGNORECASE TRUE;");

            for (String sqlStr : sqls) {
                System.out.println(sqlStr);
                stmt.executeUpdate(sqlStr);
            }

            sb = new StringBuilder();
            sb.append("create table PS_COURSE_TBL");
            sb.append("(");
            sb.append("  course             VARCHAR(6) not null,");
            sb.append("  course_title       VARCHAR(80) not null,");
            sb.append("  descr              VARCHAR(30) not null,");
            sb.append("  course_status      VARCHAR(1) not null,");
            sb.append("  course_type        VARCHAR(1) not null,");
            sb.append("  crse_multilng_sw   VARCHAR(1) not null,");
            sb.append("  internal_external  VARCHAR(1) not null,");
            sb.append("  sessn_administratn VARCHAR(1) not null,");
            sb.append("  descrshort         VARCHAR(10) not null,");
            sb.append("  creation_dt        TIMESTAMP,");
            sb.append("  revision_dt        TIMESTAMP,");
            sb.append("  primary_del_method VARCHAR(1) not null,");
            sb.append("  instr_cmps_acp_sw  VARCHAR(1) not null,");
            sb.append("  duration_time      DECIMAL(5,1) not null,");
            sb.append("  duration_converted DECIMAL(5,1) not null,");
            sb.append("  cost_unit_cd       VARCHAR(10) not null,");
            sb.append("  school_code        VARCHAR(10) not null,");
            sb.append("  school             VARCHAR(50) not null,");
            sb.append("  min_students       INTEGER not null,");
            sb.append("  max_students       INTEGER not null,");
            sb.append("  currency_cd        VARCHAR(3) not null,");
            sb.append("  education_units    DECIMAL(4,1) not null,");
            sb.append("  non_catalogued     VARCHAR(1) not null,");
            sb.append("  course_offering    VARCHAR(1) not null,");
            sb.append("  tw_category        VARCHAR(3) not null,");
            sb.append("  tw_sub_cat_cd      VARCHAR(3) not null,");
            sb.append("  tw_course_language VARCHAR(1) not null,");
            sb.append("  tw_crse_eligbility VARCHAR(60) not null,");
            sb.append("  tw_crse_title_chin VARCHAR(80) not null,");
            sb.append("  tw_est_sessn_cst   DECIMAL(15,2) not null,");
            sb.append("  tw_ojt             VARCHAR(1) not null,");
            sb.append("  tw_refresher       VARCHAR(1) not null,");
            sb.append("  tw_reimbursable    VARCHAR(1) not null,");
            sb.append("  tw_reimburse_amt   DECIMAL(15,2) not null,");
            sb.append("  tw_reimburse_pcent DECIMAL(5,2) not null,");
            sb.append("  tw_reimburse_type  VARCHAR(1) not null,");
            sb.append("  tw_course_mate_url VARCHAR(250) not null,");
            sb.append("  tw_pretest_url     VARCHAR(250) not null,");
            sb.append("  tw_after_sessn_url VARCHAR(250) not null,");
            sb.append("  tw_validity_duratn INTEGER not null,");
            sb.append("  oprid              VARCHAR(30) not null,");
            sb.append("  deptid             VARCHAR(10) not null,");
            sb.append("  tw_elearning       VARCHAR(1) not null,");
            sb.append("  tga_eligible       VARCHAR(1) not null,");
            sb.append("  tga_enterable      VARCHAR(1) not null,");
            sb.append("  tw_crse_forum      VARCHAR(3) not null,");
            sb.append("  tw_crse_maturl_flg VARCHAR(1) not null,");
            sb.append("  tw_cost_per_stud   DECIMAL(18,3) not null,");
            sb.append("  tw_duration_time   DECIMAL(5,1) not null,");
            sb.append("  tw_waive_ind       VARCHAR(1) not null,");
            sb.append("  tw_grade_ind       VARCHAR(1) not null,");
            sb.append("  tw_pass_grade      DECIMAL(5,2) not null,");
            sb.append("  lastupdoprid       VARCHAR(30) not null,");
            sb.append("  lastupddttm        TIMESTAMP(6),");
            sb.append("  tw_outsourcing     VARCHAR(1) not null,");
            sb.append("  tw_manual_check    VARCHAR(1) not null,");
            sb.append("  tw_certification   VARCHAR(1) not null,");
            sb.append("  tw_cert_type       VARCHAR(1) not null,");
            sb.append("  tw_elrn_pass_grade DECIMAL(5,2) not null,");
            sb.append("  trn_imputable_flg  VARCHAR(1) not null,");
            sb.append("  trn_eddf_flg       VARCHAR(1) not null,");
            sb.append("  trn_time_req_fra   INTEGER not null,");
            sb.append("  trn_dif_fra_flg    VARCHAR(1) not null,");
            sb.append("  descrlong          CLOB,");
            sb.append("  tw_objective       VARCHAR(1500) default ' ' not null,");
            sb.append("  tw_outline         VARCHAR(1500) default ' ' not null,");
            sb.append("  tw_retrain_ind     VARCHAR(1) default 'N' not null,");
            sb.append("  tw_crse_waive_rsn  VARCHAR(90) default ' ' not null,");
            sb.append("  PRIMARY KEY (course)");
            sb.append(")");
            
            sql = sb.toString();
            System.out.println(sql);
            stmt.executeUpdate(sql);
            
            
            sb = new StringBuilder();
            sb.append("create table PS_CRSE_SESSN_TBL");
            sb.append("(");
            sb.append("  course             VARCHAR(6) not null,");
            sb.append("  session_nbr        VARCHAR(4) not null,");
            sb.append("  session_status     VARCHAR(1) not null,");
            sb.append("  course_start_dt    TIMESTAMP,");
            sb.append("  course_end_dt      TIMESTAMP,");
            sb.append("  rescheduled        VARCHAR(1) not null,");
            sb.append("  duration_time      DECIMAL(5,1) not null,");
            sb.append("  duration_converted DECIMAL(5,1) not null,");
            sb.append("  duration_unit_cd   VARCHAR(10) not null,");
            sb.append("  sessn_start_time   TIMESTAMP(6),");
            sb.append("  sessn_end_time     TIMESTAMP(6),");
            sb.append("  session_language   VARCHAR(8) not null,");
            sb.append("  min_students       INTEGER not null,");
            sb.append("  max_students       INTEGER not null,");
            sb.append("  sessn_organizer    VARCHAR(1) not null,");
            sb.append("  deptid             VARCHAR(10) not null,");
            sb.append("  tw_sessn_owner     VARCHAR(8) not null,");
            sb.append("  tw_enrol_close_dt  TIMESTAMP not null,");
            sb.append("  tw_enrol_cancel_dt TIMESTAMP not null,");
            sb.append("  tw_trn_notify_days INTEGER not null,");
            sb.append("  tw_est_sessn_cst   DECIMAL(15,2) not null,");
            sb.append("  tw_resched_reason  VARCHAR(60) not null,");
            sb.append("  tw_location_reason VARCHAR(60) not null,");
            sb.append("  tw_trn_loc_chg     VARCHAR(1) not null,");
            sb.append("  tw_budget_owner    VARCHAR(10) not null,");
            sb.append("  trn_program        VARCHAR(6) not null,");
            sb.append("  tw_crse_cont_eval  INTEGER not null,");
            sb.append("  tw_serv_deliv_eval INTEGER not null,");
            sb.append("  tw_self_service    VARCHAR(1) not null,");
            sb.append("  tw_cancel_reason   VARCHAR(60) not null,");
            sb.append("  tw_url             VARCHAR(250) not null,");
            sb.append("  tw_budget_check    VARCHAR(1) not null,");
            sb.append("  tw_sessn_creatn_dt TIMESTAMP,");
            sb.append("  tw_site_code       VARCHAR(4) not null,");
            sb.append("  tw_sessn_remark    VARCHAR(60) not null,");
            sb.append("  tw_cost_per_stud   DECIMAL(18,3) not null,");
            sb.append("  tw_prereqcrse_flag VARCHAR(1) not null,");
            sb.append("  tw_trn_jg_flag     VARCHAR(1) not null,");
            sb.append("  tw_emailsessw_flag VARCHAR(1) not null,");
            sb.append("  tw_roi_eval        INTEGER not null,");
            sb.append("  PRIMARY KEY (course, session_nbr)");
            sb.append(")");
            
            sql = sb.toString();
            System.out.println(sql);
            stmt.executeUpdate(sql);
            
            System.out.println("init database successfully...");

            // commit and close
            conn.commit();

            conn.setAutoCommit(true);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("fail to init database");
        }

    }
    
    /**
     * test method
     * 
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {

        String driver = "org.hsqldb.jdbcDriver";
        String url = "jdbc:hsqldb:hsql://localhost/test";
        String user = "sa";
        String password = "";
        
        Connection conn = JDBCDataSource.getConnection(driver, user, password, url);
        
        // initial connection
        HSQLDatabaseInitTool init = new HSQLDatabaseInitTool();

        if (args.length == 0) {

            // initial database
            init.initiKOSDB(conn);
            
        } else {
            if (args[0].equalsIgnoreCase("init")) {
                // initial database
                init.initiKOSDB(conn);
            }
        }
        
        //insertAdmin(conn);
        
        conn.close();

    }

}