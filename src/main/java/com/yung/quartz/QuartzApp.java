package com.yung.quartz;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.triggers.AbstractTrigger;
import org.quartz.impl.triggers.CronTriggerImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yung.quartz.job.TestJob;

/**
 * A Quartz app to run job
 * 
 * @author Yung Long Li
 *
 */
public class QuartzApp {

    private static final Logger logger = LoggerFactory.getLogger(QuartzApp.class);

    /**
     * run quartz job
     */
    public void runQuartz() {

        Scheduler scheduler = null;
        try {

            // Grab the Scheduler instance from the Factory
            scheduler = StdSchedulerFactory.getDefaultScheduler();

            // and start it off
            scheduler.start();

            // define the job and tie it to our HelloJob class
            JobDetail testJob = new JobDetailImpl();
            ((JobDetailImpl) testJob).setName("lmsJob");
            ((JobDetailImpl) testJob).setGroup("group1");
            ((JobDetailImpl) testJob).setJobClass(TestJob.class);

            AbstractTrigger<?> trigger2 = new CronTriggerImpl();
            trigger2.setName("trigger2");
            trigger2.setGroup("group1");
            trigger2.setJobKey(testJob.getKey());
            ((CronTriggerImpl) trigger2).setCronExpression("0 25 15 * * ?");
            
            // Tell quartz to schedule the job using our trigger
            scheduler.scheduleJob(testJob, trigger2);
            
            logger.info("quartz start! ");

        } catch (Exception e) {
            logger.error(e.toString(), e);
        } finally {
            if (scheduler != null) {
                // try {
                // scheduler.shutdown();
                // logger.info("quartz shutdown! ");
                // } catch (SchedulerException e) {
                // e.printStackTrace();
                // }
            }
        }

    }

    /**
     * stop quartz job
     */
    public void stopQuartz() {

        Scheduler scheduler = null;

        try {

            // Grab the Scheduler instance from the Factory
            scheduler = StdSchedulerFactory.getDefaultScheduler();

            // and start it off
            if (scheduler != null) scheduler.shutdown(false);

        } catch (SchedulerException e) {
            logger.error("quartz fail!", e);
        }

    }

}