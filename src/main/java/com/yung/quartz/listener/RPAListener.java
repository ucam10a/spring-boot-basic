package com.yung.quartz.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.yung.web.test.RPACommandClient;

/**
 * Listener for quartz to start and stop
 * 
 * @author Yung Long Li
 *
 */
@WebListener()
public class RPAListener implements ServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        RPACommandClient.cancelAllProcess();
    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
    }

}