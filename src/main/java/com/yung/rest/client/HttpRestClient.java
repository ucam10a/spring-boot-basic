/**
 * @author D063124
 * @version 1.0 9/24/2014
 */

package com.yung.rest.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.bind.DatatypeConverter;

public class HttpRestClient extends RestClient {
	
	/**
	 * constructor the parameters serviceUrl, user, password could be used to
	 * enter these via the ui
	 * 
	 * @param personId
	 * @param ui
	 * @param serviceUrl
	 * @param user
	 * @param password
	 */
	public HttpRestClient(String serviceURL, String username, String password) {
	    this.serviceURL = serviceURL;
	    this.username = username;
	    this.password = password;
	}

	/**
	 * this method builds a connection and opens all necessary streams
	 * 
	 * @return
	 * @throws IOException
	 */
	public String buildConn(Map<String, String> paramMap, String method) throws IOException {
		String code = "Basic " + base64Encoode(username + ":" + password);
		StringBuilder sb = new StringBuilder(serviceURL + "?");
		boolean start = true;
		for (Entry<String, String> entry : paramMap.entrySet()) {
		    if (start == false) {
		        sb.append("&");
		    }
		    String key = URLEncoder.encode(entry.getKey(), "utf-8");
		    String value = URLEncoder.encode(entry.getValue(), "utf-8");
		    sb.append(key);
		    sb.append("=");
		    sb.append(value);
		    start = false;
		}
		URL absU = new URL(sb.toString());
		HttpsURLConnection con = (HttpsURLConnection) absU.openConnection();
		con.setRequestMethod(method);
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Authorization", code);
		BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
		StringBuffer response = new StringBuffer();
		String line;
		while ((line = reader.readLine()) != null) {
			response.append(line);
		}
		String json = response.toString();
		return json;
	}
	
	private static String base64Encoode(String input) throws UnsupportedEncodingException {
        byte[] message = input.getBytes("UTF-8");
        String encoded = DatatypeConverter.printBase64Binary(message);
        return encoded;
    }
	
}