package com.yung.rest.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.Map;
import java.util.Map.Entry;

import com.yung.tool.FileUtil;

public class MockRestClient extends RestClient {

	private static String base = "G:/YLLIZH/code/ECODataAPIsInJava";
	
	/**
	 * constructor the parameters serviceUrl, user, password could be used to
	 * enter these via the ui
	 * 
	 * @param personId
	 * @param ui
	 * @param serviceUrl
	 * @param user
	 * @param password
	 */
	public MockRestClient(String serviceURL, String username, String password) {
	    this.serviceURL = serviceURL;
	    this.username = username;
	    this.password = password;
	}
	
	/**
     * get MD5
     * 
     * @param input
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static String getMD5(String input) {
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.reset();
            m.update(input.getBytes());
            byte[] digest = m.digest();
            BigInteger bigInt = new BigInteger(1, digest);
            String hashtext = bigInt.toString(16);
            // Now we need to zero pad it if you actually want the full 32 chars.
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
	
	@Override
	public String buildConn(Map<String, String> paramMap, String method) throws IOException {
		
		StringBuilder sb = new StringBuilder();
		boolean start = true;
		for (Entry<String, String> entry : paramMap.entrySet()) {
		    if (start == false) {
		        sb.append("&");
		    }
		    String key = URLEncoder.encode(entry.getKey(), "utf-8");
		    String value = URLEncoder.encode(entry.getValue(), "utf-8");
		    sb.append(key);
		    sb.append("=");
		    sb.append(value);
		    start = false;
		}
		String hash = getMD5(sb.toString());
		File txt = FileUtil.getFile(base + "/" + hash + ".txt");
		if (!txt.exists()) {
			throw new RuntimeException(base + "/" + hash + ".txt not found");
		}
		return readFile(txt);
	}

	/**
     * read text file to String
     * 
     * @param file
     *            text file
     * @return string
     * @throws IOException
     */
    private static String readFile(File file) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(file));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }
	
}