package com.yung.rest.client;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.sql.Clob;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.yung.entity.EntityRaw;
import com.yung.entity.ann.ColumnInfo;
import com.yung.entity.ann.SchemaInfo;
import com.yung.tool.ClobConverter;
import com.yung.tool.ConcurrentTerminationTool;
import com.yung.tool.DecimalTool;
import com.yung.tool.NumberConverter;
import com.yung.tool.PlainObjectOperator;
import com.yung.tool.TimeConverter;
import com.yung.tool.UTCDateTool;

public abstract class RestClient {

	protected static final NumberConverter nConverter = new NumberConverter();
	protected static final TimeConverter tConverter = new TimeConverter();
	protected static final ClobConverter clobConverter = new ClobConverter();
	
	private static int CONCURRENT_POOL_SIZE = 10;
	
	/**
	 * this variable represents the absolute Url
	 */
	protected String serviceURL;
	
	protected String username;
	
	protected String password;
	
	public <T> List<T> fetchData(Class<T> cls, Map<String, String> paramMap) {
		try {
			String json = buildConn(paramMap, "GET");
			return parseData(json, cls);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public <T> List<T> fetchDataConcurrent(Class<T> cls, List<Map<String, String>> paramMapList) {
		try {
			List<T> list = new CopyOnWriteArrayList<T>();
			CopyOnWriteArrayList<Runnable> taskList = new CopyOnWriteArrayList<Runnable>();
			for (Map<String, String> paramMap : paramMapList) {
				Runnable task = new Runnable() {
					@Override
					public void run() {
						try {
							String json = buildConn(paramMap, "GET");
							List<T> ret = parseData(json, cls);
							list.addAll(ret);
						} catch (Exception e) {
							throw new RuntimeException(e);
						}
					}
				};
				taskList.add(task);
			}
			ConcurrentTerminationTool.executeAndWaitTermination(CONCURRENT_POOL_SIZE, taskList, 30 * 60);
			return list;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public abstract String buildConn(Map<String, String> paramMap, String method) throws IOException;
	
	private Map<String, Object> getResponseContent(String json) throws IOException {
		Type typeOf = new TypeToken<Map<String, Object>>(){}.getType();
		Map<String, Object> map = new Gson().fromJson(json, typeOf);
		return map;
	}
	
	/**
	 * this method reads the server response and parses the JSON
	 * 
	 * @param in
	 * @throws Exception
	 */
	public <T> List<T> parseData(String json, Class<T> cls) throws Exception {
		List<T> list = new ArrayList<T>();
		List<Map<String, Object>> listMap = parseData(json);
		for (Map<String, Object> map : listMap) {
			T bean = MappingColumn(cls, map);
			list.add(bean);
		}
		return list;
	}
	
	public static <T> T MappingColumn(Class<T> cls, Map<String, Object> map) throws Exception {
		T ret = cls.newInstance();
		Object id = null;
		Field[] fields = cls.getDeclaredFields();
		ConcurrentHashMap<String, Field> fieldMap = new ConcurrentHashMap<String, Field>();
		ConcurrentHashMap<String, Field> keyFieldMap = new ConcurrentHashMap<String, Field>();
		Class<?> idCls = null;
		Field idField = null;
		for (Field f : fields) {
		    ColumnInfo cInfo = f.getAnnotation(ColumnInfo.class);
		    EmbeddedId idInfo = f.getAnnotation(EmbeddedId.class);
		    if (cInfo != null) {
		        fieldMap.put(cInfo.name(), f);
		    }
		    if (idInfo != null) {
		        idCls = f.getType();
		        idField = f;
		    }
		}
		for (Field f : fields) {
            Column c = f.getAnnotation(Column.class);
            if (c != null) {
                String dbName = c.name();
                if (!fieldMap.containsKey(dbName)) {
                    fieldMap.put(dbName, f);
                }
            }
        }
		if (idCls != null) {
		    id = idCls.newInstance();
            Field[] keyFields = idCls.getDeclaredFields();
            for (Field f : keyFields) {
                ColumnInfo cInfo = f.getAnnotation(ColumnInfo.class);
                if (cInfo != null) {
                    String dbName = cInfo.name();
                    if (!fieldMap.containsKey(dbName)) {
                        keyFieldMap.put(dbName, f);
                    }
                }
            }
            for (Field f : keyFields) {
                Column c = f.getAnnotation(Column.class);
                if (c != null) {
                    String dbName = c.name();
                    if (!fieldMap.containsKey(dbName)) {
                        keyFieldMap.put(dbName, f);
                    }
                }
            }
        }
		List<String> numberFormatErros = new ArrayList<String>();
		List<String> timeFormatErros = new ArrayList<String>();
		for (Entry<String, Field> entry : keyFieldMap.entrySet()) {
		    setValue(entry, map, numberFormatErros, timeFormatErros, id);
		}
		if (idField != null) {
		    PlainObjectOperator.runSetter(ret, idField.getName(), id);
		}
		for (Entry<String, Field> entry : fieldMap.entrySet()) {
		    setValue(entry, map, numberFormatErros, timeFormatErros, ret);
		}
		if (ret instanceof EntityRaw) {
			if (numberFormatErros.size() > 0) {
				for (String error : numberFormatErros) {
					String entityInfo = ((EntityRaw) ret).getEntityInfo();
					DecimalTool.logError(entityInfo, error);
				}
			}
			if (timeFormatErros.size() > 0) {
				for (String error : timeFormatErros) {
					String entityInfo = ((EntityRaw) ret).getEntityInfo();
					UTCDateTool.logError(entityInfo, error);
				}
			}
		}
		return ret;
	}
	
	private static void setValue(Entry<String, Field> entry, Map<String, Object> map, List<String> numberFormatErros, List<String> timeFormatErros, Object obj) throws Exception {
	    String dbCname = entry.getKey();
        Field f = entry.getValue();
        Type type = f.getType();
        String fieldName = f.getName();
        SchemaInfo sch = f.getAnnotation(SchemaInfo.class);
        Object val = map.get(dbCname);
        if (val == null || "".equals(val)) {
            val = checkDefault(val, sch, type);
        }
        if (val != null) {
            if (val.getClass() != String.class || !"".equals(val.toString())) {
                if (nConverter.isNumber(val.getClass()) && nConverter.isNumber(type)) {
                    val = nConverter.Convert(val, type);
                } else if (val.getClass() == String.class && nConverter.isNumber(type)) {
                    if (NumberConverter.isNumeric(val.toString())) {
                        val = new BigDecimal(val.toString());
                    } else {
                        numberFormatErros.add("[" + fieldName + "]'" + val.toString() + "' is not number!");
                        return;
                    }
                } else if (tConverter.isTime(val.getClass()) && tConverter.isTime(type)) {
                    val = tConverter.Convert(val, type);
                } else if (val.getClass() == String.class && tConverter.isTime(type)) {
                    Object dt =  UTCDateTool.parseString(val.toString());
                    if (dt instanceof java.util.Date) {
                        val = tConverter.Convert(dt, type);
                    } else if (dt.getClass() == String.class) {
                        String error = dt.toString();
                        timeFormatErros.add("[" + fieldName + "]" + error);
                        return;
                    } else {
                        throw new Exception("dt: " + dt.getClass() + " is not java.util.Date or String!");
                    }
                } else if (type == String.class && val.getClass() != String.class) {
                    if (val.getClass() == Clob.class) {
                        val = clobConverter.Convert(val, String.class);
                    } else {
                        val =  val.toString();
                    }
                }
                PlainObjectOperator.runSetter(obj, fieldName, val);
            }
        }
	}

	public static Object checkDefault(Object src, SchemaInfo sch, Type typeRef) throws Exception {
		if (sch == null) {
			return src;
		}
		String type = sch.type();
		if ("string".equals(type)) {
			if (src == null || "".equals(src)) {
				if (sch.allowNull() == false) {
					src = sch.defaultString();
				}
			}
		} else if ("number".equals(type)) {
			if (src == null || "".equals(src)) {
				if (sch.allowNull() == false) {
					BigDecimal defaultNumber = new BigDecimal(sch.defaultNumber());
					src = nConverter.Convert(defaultNumber, typeRef);
				}
			}
		} else if ("date".equals(type)) {
			if (src == null || "".equals(src)) {
				if (sch.allowNull() == false) {
					String defaultTime = sch.defaultTime();
					if (!"".equals(defaultTime)) {
						Object dt = UTCDateTool.parseString(defaultTime);
						src = tConverter.Convert(dt, typeRef);
					}
				}
			}
		}
		return src;
	}
	
	public List<Map<String, Object>> parseData(String json) throws Exception {
		Map<String, Object> map = getResponseContent(json);
		@SuppressWarnings("unchecked")
		List<Map<String, Object>> list = (List<Map<String, Object>>) map.get("data");
		return list;
	}
	
}