package com.yung.service;

import java.util.Date;
import java.util.concurrent.Future;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

@EnableAsync
@Service
public class AsyncService {

    @Async
    public void asyncMsg() throws Exception {
        Thread.sleep(5000L);
        System.out.println("async:" + new Date() + ", name:" + Thread.currentThread().getName());
    }
    
    public void syncMsg() throws Exception {
        Thread.sleep(5000L);
        System.out.println("sync:" + new Date() + ", name:" + Thread.currentThread().getName());
    }
    
    @Async
    public Future<String> asyncMethodWithReturnType() {
        System.out.println("Execute method asynchronously - " + Thread.currentThread().getName());
        try {
            Thread.sleep(5000);
            return new AsyncResult<String>("hello async world !!!!");
        } catch (InterruptedException e) {
            // swallow exception
        }
        return null;
    }
    
    public String syncMethodWithReturnType() {
        System.out.println("Execute method synchronously - " + Thread.currentThread().getName());
        try {
            Thread.sleep(5000);
            return "hello sync world !!!!";
        } catch (InterruptedException e) {
            // swallow exception
        }
        return null;
    }
    
    
}
