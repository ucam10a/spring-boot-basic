<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>RPA</title>
    <link href="css/yung-util.css" rel="stylesheet" type="text/css"/>
    <script src="js/resources/jquery-1.7.2.js"></script>
    <script src="js/yung-full.js"></script>
    <script>
        $Y.def("com.yung.util.*");
        function viewLog() {
	    	$("#logDiv").show();
	    	var ajaxhttp = new $Y.AjaxHttp('rest/viewRPALog', 'rpaForm', function callback(response, self)
	        {
	            var errormsg = response.errormsg;
	            if(errormsg != ''){
	                alert(errormsg);
	                return;
	            } else {
	            	$("#RPALogWindow").val(response.logMessage);
	            }
	        });
	        ajaxhttp.send();
	    }
        function cancel(pid) {
        	var ret = confirm("Cancel Process?");
        	if (ret == false) {
        		return;
        	}
        	$("#rpaForm-pid").val(pid);
	    	var ajaxhttp = new $Y.AjaxHttp('rest/cancel', 'rpaForm', function callback(response, self)
	        {
	            var errormsg = response.errormsg;
	            if(errormsg != ''){
	                alert(errormsg);
	                return;
	            } else {
	            	alert("Cancel Successfully!");
	            	viewLog();
	            }
	        });
	        ajaxhttp.send();
	    }
    </script> 
</head>
<body>

    <div id="center_view">
        <br/>
        <c:out value="${htmlMsg}" escapeXml="false" />
        <br/>
        <div id="logDiv" style="display: none;">
            <textarea id="RPALogWindow" rows="20" cols="120" ></textarea>
        </div>
    </div>
    <dir style="display: none;">
        <form id="rpaForm" name="rpaForm" >
            <input id="rpaForm-jarName" name="jarName" type="hidden" value="<c:out value="${jarName}"/>" />
            <input id="rpaForm-jarName" name="logNo" type="hidden" value="<c:out value="${logNo}"/>" />
            <input id="rpaForm-pid" name="pid" type="hidden" value="" />
        </form>
    </dir>
</body>
</html>