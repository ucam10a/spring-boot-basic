<%@ page language="java" contentType="text/html; charset=UTF8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<html>
<head>
    <link href='css/yung-util.css' rel='stylesheet' type='text/css'/>
    <script src="js/resources/jquery-1.7.2.js" ></script>
    <script src="js/resources/jquery-json.js"></script> <!-- fixed only for old IE -->
    <script src="js/yung-JS-extend.js"></script>
    <script src="js/yung-Calendar.js"></script>
    <script src="js/yung-Collection.js"></script>
    <script src="js/yung-Map.js"></script>
    <script src="js/base64-string.js" ></script>
    <script src="js/lz-string.js" ></script>
    <script src="js/yung-Validator.js" ></script>
    <script src="js/yung-Movable.js" ></script>
    <script src="js/yung-Popup.js" ></script>
    <script src="js/yung-AJAX.js" ></script>
    <script src="js/yung-FloatIframe.js" ></script>
    <script src="js/yung-FloatDiv.js" ></script>
    <script src="js/yung-Position.js" ></script>
    <script src="js/yung-Toolbar.js" ></script>
    <script src="js/yung-BasicTemplate.js" ></script>
    <script src="js/yung-Tab.js" ></script>
    <script src="js/yung-StringUtils.js" ></script>
    <script src="js/yung-TableGridUtil.js" ></script>
    <script src="js/yung-AudioRecorder.js" ></script>
    <script src="js/yung-Combo.js" ></script>
    <script src="js/yung-DatePicker.js" ></script>
    <script>
    
        $Y.def("com.yung.util.*");
		
        var schemaArray = [];
        var id = new $Y.TableGridSchema("id", "30", "id");
        id.setProperty('columnHidden', true);
        schemaArray.push(id);
        
        for (var i = 0; i < 10; i++) {
        	var schema = new $Y.TableGridSchema("header" + i, "100", "header" + i);
            if (i % 10 == 0) {
                schema.setProperty("type", "ed");
                var list = ['create', 'simple', 'drop-down', 'list', 'items', 'user', 'input', 'html', 'forms', 'select', 'box', 'called', 'option', 'options', 'form', 'tag'];
                schema.setProperty("columnValidator", list);
            }
            if ((i + 1) % 7 == 0) {
                schema.setProperty("type", "date");
            }
        	schemaArray.push(schema);
        }
        var lzColumnInfo = $Y.TableGridSchema.convertToLZString(schemaArray);
        
        var tableData = {};
        var rows = [];
        for (var i = 0; i < 500; i++) {
        	var content = {};
        	for (var j = 0; j < schemaArray.length; j++) {
        		var schema = schemaArray[j];
        		content[schema.getProperty("columnId")] = "cell" + i + j; 
        	}
        	rows.push(content);
        }
        
        tableData["rows"] = rows;
        
        var grid = null;
        $( document ).ready(function() {
        	
            var columnUtil = new $Y.ColumnUtil(lzColumnInfo);
            grid = $Y.TableGridUtil.instance('gridDiv', columnUtil);
            grid.loadData(tableData.rows);
            
            grid.setComment("cell1000", "header6", "abc");
            grid.setComment("cell2000", "header4", "abc");
            grid.setComment("cell3000", "header4", "abc");
            
        });
	    
    </script>
</head>
<body>
    <br><br>
    <table style="width: 100%;">
        <tr><td align="center">
            <div id="gridDiv" style="width: 1200px; height: 500px;" >
            </div>
        </td></tr>
    </table>
    
</body>
</html>