package com.yung;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.yung.service.AsyncService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceTest {
    
	private static Logger logger = Logger.getLogger(ServiceTest.class.getName());
	
	@Autowired
	AsyncService asyncService;
	
    @Test
    public void testService() {
        
        try {
            
            System.out.println("start syncMsg ...");
            asyncService.syncMsg();
            System.out.println("end syncMsg");
            
            System.out.println("start asyncMsg ...");
            asyncService.asyncMsg();
            System.out.println("end asyncMsg");
            
            System.out.println("start syncMethodWithReturnType ...");
            String ret = asyncService.syncMethodWithReturnType();
            System.out.println("syncMethodWithReturnType: " + ret);
            System.out.println("end syncMethodWithReturnType");
            
            System.out.println("start asyncMethodWithReturnType ...");
            Future<String> asyncRet = asyncService.asyncMethodWithReturnType();
            ret = awaitFuture(asyncRet, 10);
            
            System.out.println("asyncMethodWithReturnType: " + ret);
            System.out.println("end asyncMethodWithReturnType");
        	
        } catch (Exception e) {
        	logger.log(Level.SEVERE, e.toString(), e);
        }
        
    }
    
    public static <T> T awaitFuture(Future<T> future, int waitSeconds) throws InterruptedException, ExecutionException {
        long start = System.currentTimeMillis();
        while (true) {
            if (future.isDone()) {
                return future.get();
            }
            if ((System.currentTimeMillis() - start) > (waitSeconds * 1000)) {
                break;
            }
            Thread.sleep(100);
        }
        if (future.isDone()) {
            return future.get();
        }
        return null;
    }
    
}